/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins.quorum.Libraries.Sound.IOS;

import plugins.quorum.Libraries.Sound.Audio;
import static plugins.quorum.Libraries.Sound.Audio.loader;

// from audio.java
import plugins.quorum.Libraries.Sound.Data;
import plugins.quorum.Libraries.Sound.DataLoader;

import quorum.Libraries.System.File_;
import quorum.Libraries.Sound.AudioSamples_;

 
import java.lang.Object;
// import sun.audio;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import org.robovm.apple.audiotoolbox.AudioFileStream;
import org.robovm.apple.audiotoolbox.AudioFileStreamParseFlags;
import org.robovm.apple.audiotoolbox.AudioFileType;

// from apple audiotoolbox
import org.robovm.apple.foundation.*;
import org.robovm.apple.coreaudio.*;
import org.robovm.apple.coreaudio.AudioBuffer;
import org.robovm.apple.avfoundation.*;
import org.robovm.apple.uikit.*;
import sun.audio.AudioData;
import sun.audio.AudioDataStream;
import sun.audio.AudioPlayer;


import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.robovm.apple.corefoundation.OSStatusException;



/**
 *
 * @author alleew
 */
public class IOSLoader implements DataLoader
        
        
{
    private static Data data;
    public java.lang.Object me_ = null;
    
    @Override
    public Data Load(File_ quorumFile) 
    {
        // "eroneus error" - not sure if this is something with netbeans or if IOSSoundData just isnt declared correctly
        return new IOSSoundData(quorumFile);
    }

    @Override
    public Data LoadToStream(File_ quorumFile) 
    {
        String path = quorumFile.GetPath().replace('\\', '/');
        OALAudioTrack track = OALAudioTrack.create();
        if (track != null) 
        {
            if (track.preloadFile(path)) 
            {
                return new IOSStreamData(track);
            }
        }
        throw new RuntimeException("Error opening audio file at " + path);
    }
    
    @Override
    public Data Load(quorum.Libraries.Sound.AudioSamples_ audioSamples)
    {
        // loads files that can be played normally for ios      
        // james working on this
        return null;
   
    }

    private ByteBuffer putShort(short s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public interface ParseListener {
        void onPropertyParsed(AudioFileStream audioFileStream, AudioFileStreamProperty property, AudioFileStreamMutablePropertyFlags flags);

        void onPacketsParsed(int numberBytes, long inputData, AudioStreamPacketDescription[] packetDescriptions);
    }
    
    private static java.util.concurrent.atomic.AtomicLong callbackId = new java.util.concurrent.atomic.AtomicLong();
    
    private static LongMap<ParseListener> parseListeners = new LongMap<>();
    
    @Override
    public Data LoadToStream(quorum.Libraries.Sound.AudioSamples_ audioSamples)
    {
        // loads files that can be streamed for ios
        
//        // need to convert audioSamples into a type of OAL track
//        OALAudioTrack convertedSamples;
//        
//        // using abstract function QueueSamples() from Data.java
//        convertedSamples = OALAudioTrack.create();
//       // IOSStreamData.FileToStream(convertedSamples);
//        
//       return new IOSStreamData(convertedSamples); 
//       // return Data.FileToStream(convertedSamples);
        
//       throw new UnsupportedOperationException("Direct AudioBuffer manipulation is not currently supported on this platform.");
        
// -----------------------------------------------------------------------------
// trying to use ios audiofilestream instead to work with the audiosamples buffer
        
       // 1. create a new audio file stream parser by calling the AudioFileStreamOpen function. 
//       private static LongMap<ParseListener> parseListeners = new LongMap<>();
       
       AudioFileStream parser = AudioFileStream.open(parseListeners, audioSamples);
        
       // 2. have data to pass to the parser. Send the data to the parser sequentially and, if possible, without gaps.
        // - When the parser acquires a usable buffer of audio data, it invokes your audio data callback. 
        // Your callback can then play the data, write it to a file, or otherwise process it.
        // - When the parser acquires metadata, it invokes your property callback—which in turn can 
        // obtain the property value by calling the AudioFileStreamGetPropertyInfo and AudioFileStreamGetProperty functions.
        
        // jame's code to convert short buffer to a byte buffer
        // need to pass in a byte buffer into this parseBytes function
        
        //        1. convert short[] to byte[]
        //        2. read the byte array to the speakers

        // initialize audio format
        AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
        SourceDataLine speakers;

        int shortRead = 0;
        int bufferSize = 0;
        int i = 0;

        short[] buffer = buffer; // this returns the buffer

        // convert shortBuffer to byteBuffer
        bufferSize = audioSamples.GetTotalSize();
        short[] short_buffer_array = buffer;
        ByteBuffer byte_buffer = ByteBuffer.allocate((bufferSize*2));  // return buffer size
        while(bufferSize>=i)
        {
            byte_buffer = putShort(short_buffer_array[i]);
            i++;
        }

        byte[] byte_buffer_array = new byte[bufferSize*2];
        byte_buffer_array = byte_buffer.array();
        // finished converting
        
        
        
        try {
            // nothing?

        parser.parseBytes(byte_buffer_array, AudioFileStreamParseFlags.None);
        } catch (OSStatusException ex) {
            Logger.getLogger(IOSLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         // 3. When finished parsing a stream, call the AudioFileStreamClose function to close and deallocate the parser.
         
         // 
        try {
            parser.close();
        } catch (OSStatusException ex) {
            Logger.getLogger(IOSLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       return null;
    }
}
