package Libraries.Interface.Controls

use Libraries.Interface.Item2D
use Libraries.Interface.Events.FocusEvent
use Libraries.Interface.Events.TreeChangeEvent
use Libraries.Interface.Events.TreeChangeListener
use Libraries.Interface.Layouts.Layout
use Libraries.Interface.Layouts.TreeLayout
use Libraries.Interface.Layouts.FlowLayout
use Libraries.Interface.Layouts.LayoutProperties
use Libraries.Interface.Selections.TreeSelection
use Libraries.Interface.AccessibilityManager
use Libraries.Containers.Array
use Libraries.Containers.Iterator
use Libraries.Game.GameStateManager
use Libraries.Interface.Views.LabelBoxView
use Libraries.Game.Graphics.Color

class Tree is Control
    Control panel
    Layout panelLayout = undefined
    TreeSelection selection
    Array<TreeItem> treeItems
    Array<TreeChangeListener> listeners
    GameStateManager manager

    on create
        SetFocusable(true)
        SetInputGroup("Tree")
        TreeLayout flow
        
        // Hardcoded value sufficient for basic cases. Should be replaced with
        // something more responsive to design in the future.
        flow:SetPadding(2)
        flow:SetTopAligned(true)

        panelLayout = flow
        panel:SetLayout(flow)
        panel:SetName("Tree Panel")
        parent:Item2D:Add(panel)

        selection:Initialize(me)
        SetAccessibilityCode(parent:Item:TREE)

        LayoutProperties properties = GetDefaultLayoutProperties()
        properties:SetHorizontalLayoutMode(properties:FIT_CONTENTS)
        properties:SetVerticalLayoutMode(properties:FIT_CONTENTS)

        FlowLayout layout
        SetLayout(layout)

        LayoutProperties panelProperties = panel:GetDefaultLayoutProperties()
        panelProperties:SetHorizontalLayoutMode(panelProperties:FIT_CONTENTS)
        panelProperties:SetVerticalLayoutMode(panelProperties:FIT_CONTENTS)
    end

    action IsAccessibleParent returns boolean
        return true
    end

    action Add(TreeItem item)
        item:SetTree(me)
        treeItems:Add(item)
        panel:Add(item)
    end

    action Add(integer location, TreeItem item)
        item:SetTree(me)
        treeItems:Add(location, item)
        panel:Add(location, item)
    end
    
    action Remove(TreeItem item)
        // If the removed TreeItem was part of the selected path, empty the
        // selection (rather than maintaining it on a removed item).
        if selection:GetPath():Get(0) = item
            Deselect()
        end

        panel:Remove(item)
        treeItems:Remove(item)
        item:SetTree(undefined)
    end

    action GetTreeItems returns Iterator<TreeItem>
        return treeItems:GetIterator()
    end    

    /*
    This action returns the number of top-level TreeItems contained in the Tree,
    or in other words, how many TreeItems have been directly added to the Tree.
    This does not include the number of sub-items contained within TreeItems.

    Attribute: Returns The number of top-level TreeItems contained in the Tree.
    */
    action GetSize returns integer
        return treeItems:GetSize()
    end

    action GetSelection returns TreeSelection
        return selection
    end

    action GetMultipleSelection returns Array<TreeSelection>
        return undefined
    end

    action HasMultipleSelection returns boolean
        return false
    end

    action Select(Array<TreeItem> path)
        Array<TreeItem> pathCopy = path:CopyToArray()
        TreeItem first = undefined

        if not path:IsEmpty()
            first = pathCopy:RemoveFromFront()
        end

        Array<TreeItem> selectionPath = selection:GetPath()
        TreeItem oldFirst = undefined

        if not selectionPath:IsEmpty()
            oldFirst = selectionPath:RemoveFromFront()
        end

        if first not= undefined
            if first = oldFirst
                first:UpdateSelection(selectionPath, pathCopy)
            else
                if oldFirst not= undefined
                    oldFirst:RecursiveDeselect(selectionPath)
                end
                first:RecursiveSelect(pathCopy)
            end
        end

        // Set our selection object with the new path, triggering a selection
        // event.
        selection:Set(path)
    end

    action Deselect
        TreeSelection selection = GetSelection()
        Deselect(selection:GetPath())
    end

    private action Deselect(Array<TreeItem> path)
        TreeItem item = undefined
        if not path:IsEmpty()
            item = path:RemoveFromFront()
        end

        if item not= undefined
            item:RecursiveDeselect(path)
        end

        // Add the removed portion back to the path to undo our side effects.
        path:AddToFront(item)
        Array<TreeItem> newPath
        selection:Set(newPath)
    end

    action SelectionRight
        if selection:GetPath():IsEmpty()
            if not treeItems:IsEmpty()
                Array<TreeItem> array
                array:Add(treeItems:Get(0))
                Select(array)
            end
        else
            TreeItem item = selection:GetPath():GetFromEnd()
            if item not= undefined and item:IsSubtree()
                if item:IsOpen()
                    Iterator<TreeItem> iter = item:GetTreeItems()
                    if iter:HasNext()
                        Select(iter:Next():GetTreePath())
                    end
                else
                    item:Open()
                end
            end
        end
    end

    action SelectionLeft
        Array<TreeItem> path = selection:GetPath()
        if path:IsEmpty()
            if not treeItems:IsEmpty()
                SelectFirst()
            end
        else
            TreeItem item = path:GetFromEnd()
            if item not= undefined
                if item:IsSubtree() and item:IsOpen()
                    item:Close()
                elseif path:GetSize() > 1
                    path:RemoveFromEnd()
                    Select(path)
                end
            end
        end
    end

    action SelectionDown
        Array<TreeItem> path = selection:GetPath()
        if path:IsEmpty()
            if not treeItems:IsEmpty()
                SelectFirst()
            end
        else
            TreeItem item = path:GetFromEnd()
            if item not= undefined
                item:SelectionDown()
            end
        end
    end

    action SelectionUp
        Array<TreeItem> path = selection:GetPath()
        if path:IsEmpty()
            if not treeItems:IsEmpty()
                SelectFirst()
            end
        else
            TreeItem item = path:GetFromEnd()
            if item not= undefined
                item:SelectionUp()
            end
        end
    end

    action SelectionSkipForward
        Array<TreeItem> path = selection:GetPath()
        if path:IsEmpty()
            SelectFirst()
        else
            integer counter = 0
            TreeItem first = path:GetFromFront()
            repeat while counter < treeItems:GetSize()
                if treeItems:Get(counter):Equals(first)
                    integer select = counter + 1

                    if select >= treeItems:GetSize()
                        return now
                    end

                    first = treeItems:Get(select)
                    path:Empty()
                    path:Add(first)
                    Select(path)
                    return now
                end
                counter = counter + 1
            end
        end
    end

    action SelectionSkipBackward
        Array<TreeItem> path = selection:GetPath()
        if path:IsEmpty()
            SelectFirst()
        else
            integer counter = 0
            TreeItem first = path:GetFromFront()
            repeat while counter < treeItems:GetSize()
                if treeItems:Get(counter):Equals(first)
                    integer select = counter - 1

                    if select < 0
                        return now
                    end

                    treeItems:Get(select):SelectLastElement()
                    return now
                end
                counter = counter + 1
            end
        end
    end

    private action SelectFirst
        if not treeItems:IsEmpty()
            Array<TreeItem> array
            array:Add(treeItems:Get(0))
            Select(array)
        end
    end

    /*

    */
    action GainedFocus(FocusEvent event)
        if selection:IsEmpty() and not treeItems:IsEmpty()
            Array<TreeItem> array
            array:Add(treeItems:Get(0))
            Select(array)
        else
            // Force a selection event.
            selection:SetDisplayName(selection:GetDisplayName())
            if not treeItems:IsEmpty()
                selection:GetTreeItem():GainedSelection()
            end
        end
    end

    /*

    */
    action LostFocus(FocusEvent event)
        if not selection:IsEmpty()
            selection:GetTreeItem():UnfocusedSelection()
        end
    end

    action AddTreeChangeListener(TreeChangeListener listener)
        listeners:Add(listener)
    end

    action RemoveTreeChangeListener(TreeChangeListener listener)
        listeners:Remove(listener)
    end

    action NotifyTreeChangeListeners(TreeChangeEvent event)
        integer counter = 0
        if event:GetEventType() = event:OPENED
            repeat while counter < listeners:GetSize()
                listeners:Get(counter):OpenedTree(event)
                counter = counter + 1
            end
        elseif event:GetEventType() = event:CLOSED
            repeat while counter < listeners:GetSize()
                listeners:Get(counter):ClosedTree(event)
                counter = counter + 1
            end
        end

        manager:GetInput():NotifyTreeChangeListeners(event)
    end

    action Empty 
        panel:Empty()
        treeItems:Empty()
        selection:Empty()
    end

    action IsEmpty returns boolean
        return treeItems:IsEmpty()
    end
end
